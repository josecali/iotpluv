<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>
        <h1>New actuator</h1>


{{ Form::open(['url' => 'admin/actuators', 'method' => 'POST']) }}


<input type="hidden" name="_token" value="{{ csrf_token() }}">

{{ Form::label('name', 'Name') }}
{{ Form::text('name') }}

{{ Form::label('description', 'Description') }}
{{ Form::text('description') }}


{{ Form::submit('Create actuator') }}

{{ Form::close() }}


</body>
</html>
