<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>
        <h1>New actuator</h1>

{!! Form::open(['method' => 'POST', 'url' => 'admin/lecturas', 'class' => 'form-horizontal']) !!}


<input type="hidden" name="_token" value="{{ csrf_token() }}">

{{ Form::label('name', 'Name') }}
{{ Form::text('name') }}

{{ Form::label('description', 'Description') }}
{{ Form::text('description') }}

    <div class="btn-group pull-right">


        {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
        {!! Form::submit("Create actuator", ['class' => 'btn btn-success']) !!}
    </div>
{!! Form::close() !!}


</body>
</html>
