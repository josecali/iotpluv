@extends('layouts.app')

@section('content')
<div class="container">

  <title>My Charts</title>

<!-- pasar esta inicializacion a app.blade.php -->
  {!! Charts::assets() !!}


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
{{ $numero  }}
                    <center>
                        {!! $chart->render() !!}
                    </center>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
