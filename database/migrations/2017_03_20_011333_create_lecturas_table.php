<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectura', function (Blueprint $table) {
            $table->increments('id_lectura');
            $table->date('fecha');
            $table->time('hora');
            $table->double('valor');
            $table->integer('id_tipo_sensor')->unsigned();
            $table->integer('id_estacion')->unsigned();

            $table->foreign('id_tipo_sensor')->references('id_tipo_sensor')->on('tipo_sensor');
            $table->foreign('id_estacion')->references('id_estacion')->on('estacion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lectura');
    }
}
