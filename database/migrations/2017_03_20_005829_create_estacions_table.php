<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estacion', function (Blueprint $table) {
            $table->increments('id_estacion');
            $table->string('nombre_estacion' ,30);
            $table->string('abreviatura_estacion' ,5);
            $table->date('fecha_instalacion');
            $table->double('ultimo_dato');
            $table->integer('id_tipo_estacion')->unsigned();
            $table->integer('id_zona')->unsigned();
            $table->timestamps();

            $table->foreign('id_tipo_estacion')->references('id_tipo_estacion')->on('tipo_estacion');
            $table->foreign('id_zona')->references('id_zona')->on('zona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estacion');
    }
}
