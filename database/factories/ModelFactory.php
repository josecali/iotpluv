<?php
use Carbon\Carbon;
use App\Zona;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/*
$factory->define(App\Zona::class, function (Faker\Generator $faker)
{
  return [
    'nombre_zona'   => $faker->word,
    'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
  ];
});


$factory->define(App\TipoEstacion::class, function (Faker\Generator $faker)
{
  return [
    'nombre_zona' => $faker->company,
    'nombre_zona' => $faker->company,
    'nombre_zona' => $faker->company,
  ];
});
*/
