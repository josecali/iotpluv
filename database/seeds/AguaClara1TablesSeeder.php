<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Zona;

class AguaClara1TablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $faker = Faker::create();

          //DB::table('zona')->truncate();

            for ($i=0; $i < 5; $i++) {

              $zona = DB::table('zona')->insert([
                    'nombre_zona'   => $faker->word,
                    'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
                ]);

              $tipo_estacion =   DB::table('tipo_estacion')->insert([
                      'nombre_tipo_estacion' => $faker->domainWord,
                      'description' => $faker->realText($maxNbChars = 100, $indexSize = 2),
                      'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                  ]);


              $estacion = DB::table('estacion')->insert([
                    'id_tipo_estacion'      => $tipo_estacion,
                    'id_zona'               => $zona,
                    'nombre_estacion'       => $faker->domainWord,
                    'abreviatura_estacion'  => $faker->text($maxNbChars = 5),
                    'fecha_instalacion'     => $faker->dateTimeThisCentury->format('Y-m-d'),
                    'ultimo_dato'           => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = NULL),
                    'created_at'            => Carbon::now()->format('Y-m-d H:i:s')
                ]);

              $tipo_sensor = DB::table('tipo_sensor')->insert([
                    'nombre_tipo_sensor'        => $faker->domainWord,
                    'descripcion_tipo_sensor'   => $faker->realText($maxNbChars = 100, $indexSize = 2),
                    'created_at'                => Carbon::now()->format('Y-m-d H:i:s')
                ]);
/*
              DB::table('lectura')->insert([
                    'id_tipo_sensor'  => $tipo_sensor,
                    'id_estacion'     => $estacion,
                    'fecha'           => $faker->dateTimeThisCentury->format('Y-m-d'),
                    'hora'            => $faker->time($format = 'H:i:s', $max = 'now'),
                    'valor'           => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 80),
                    'created_at'      => Carbon::now()->format('Y-m-d H:i:s')
                ]);
                */
            }

 // 8567
 //
for ($i=0; $i < 55; $i++) {


  DB::table('lectura')->insert([
        'id_tipo_sensor'  => $faker->numberBetween($min = 1, $max = 5),
        'id_estacion'     => $faker->numberBetween($min = 1, $max = 5),
        //'fecha'           => $faker->dateTimeThisCentury->format('Y-m-d'),
        'fecha'           => $faker->dateTimeBetween($startDate = '-3 month', $endDate = 'now')->format('Y-m-d'),
        'hora'            => $faker->time($format = 'H:i:s', $max = 'now'),
        'valor'           => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 80),
        //'created_at'      => Carbon::now()->format('Y-m-d H:i:s')
        'created_at'      => $faker->dateTimeBetween($startDate = '-3 month', $endDate = 'now')

    ]);
}
    }
}
