<?php 
$I = new FunctionalTester($scenario);
$I->am('A laravel developer');
$I->wantTo('Know if Laravel 5 was installed successfully');
$I->amOnPage('/');
$I->see('Laravel');
