<?php
$I = new FunctionalTester($scenario);
$I->wantTo('perform actions and see result');

$I->am('a admin');
$I->wantTo('Create a new actuator');

//When
$I->amOnPage('/admin/actuators');
//And
$I->click('Add new actuator');

//Then - redirect to url add actuator
$I->seeCurrentUrlEquals('/admin/actuators/create');
// Can see a tittle

//$I->see('New actuator', 'h1');
$I->see('New actuator');

//When
$I->fillField('name', 'actuator');
// And
$I->click('Create lectura');
// Then
$I->seeCurrentUrlEquals('/admin/actuators/11');
$I->see('List actuator', '<h1>');
$I->seeRecord('actuator', ['name', 'Compuerta']);
