<?php
$I = new FunctionalTester($scenario);
$I->wantTo('perform actions and see result');

$I->am('a admin');
$I->wantTo('Create a new actuator');

//When
$I->amOnPage('/admin/actuators');
//And
$I->click('Add new actuator');

//Then - redirect to url add actuator
$I->seeCurrentUrlEquals('/admin/actuators/create');
// Can see a tittle

//$I->see('New actuator', 'h1');
$I->see('New actuator');

//When
// And
$I->fillField('name', 'actuator');
$I->fillField('name', 'descriptio');

$I->click('Create actuator');
// Then
$I->seeCurrentUrlEquals('/admin/actuators/8');
$I->see('List actuator');
$I->seeRecord('actuator', ['name', 'descriptio']);
