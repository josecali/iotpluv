<?php


namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Charts;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class TestChartController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $chart = Charts::create('line', 'highcharts')
            ->setView('custom.line.chart.view') // Use this if you want to use your own template
            ->setTitle('My nice chart')
            ->setLabels(['First', 'Second', 'Third'])
            ->setValues([5,10,20])
            ->setDimensions(1000,500)
            ->setResponsive(false);

        return view('frontend.user.dashboard',['chart'=>$chart]);
    }
}
