<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Charts;
use App\Lectura;

class ChartsController extends Controller
{
    public function index()
    {

      $numero = 950;
/*
        $chart = Charts::multi('line', 'material')
            // Setup the chart settings
            ->title("My Cool Chart")
            // A dimension of 0 means it will take 100% of the space
            ->dimensions(0, 400) // Width x Height
            // This defines a preset of colors already done:)
            ->template("material")
            // You could always set them manually
            // ->colors(['#2196F3', '#F44336', '#FFC107'])
            // Setup the diferent datasets (this is a multi chart)
            ->dataset('Element 1', [5,20,100])
            ->dataset('Element 2', [15,30,80])
            ->dataset('Element 3', query personalizado  laravel 5.3[25,10,40])
            // Setup what the values mean
            ->labels(['One', 'Two', 'Three']);
  */


$chart = Charts::database(\DB::table('agua_clara1.lectura AS lectura')
            ->join('agua_clara1.estacion AS estacion', 'lectura.id_estacion', '=', 'estacion.id_estacion')
            ->select('estacion.nombre_estacion', 'estacion.id_estacion', 'lectura.id_estacion', 'lectura.valor', 'lectura.fecha', 'lectura.created_at')
            ->get(),
              'area', 'highcharts')
              ->title('Lecturas')

              ->elementLabel('Total')
              ->responsive(false)
              ->dimensions(0, 500)
              ->groupBy('fecha')
              ->labels(['First', 'Second']);


        return view('index', [
          'chart' => $chart,
          'numero' => $numero
        ]);


    }

    public function graph()
    {

      $numero = 950;
      $chart = Charts::realtime(url('datagraph'), 1000, 'area', 'highcharts')
                  ->responsive(false)
                  ->height(300)
                  ->width(0)
                  ->title("Permissions Chart")
                  ->maxValues(10);

              return view('index', [
                'chart' => $chart,
                'numero' => $numero
              ]);
    }
}
