<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Zona;
use App\TipoEstacion;

class Estacion extends Model
{
    protected $fillable = [
      'id_estacion', 'nombre_estacion',
      'abreviatura_estacion', 'fecha_instalacion',
      'ultimo_dato', 'id_tipo_estacion', 'id_zona',
    ];

public function TipoEstacion()
{
    return $this->belongsTo(TipoEstacion::class);
}

public function Zona()
{
  return $this->belongsTo(Zona::class);
}
}
