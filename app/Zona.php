<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Estacion;

class Zona extends Model
{
    protected $fillable = [
      'nombre_zona',
    ];

    public function Estacion()
    {
      return $this->hasMany(Estacion::class);
    }
}
