<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Estacion;
use App\TipoEstacion;

class Lectura extends Model
{
    protected $fillable = [
      'fecha', 'hora', 'valor', 'id_tipo_sensor', 'id_estacion',
    ];

    public function Estacion()
    {
      return $this->belongsTo(Estacion::class);
    }

    public function TipoEstacion()
    {
      return $this->belongsTo(TipoEstacion::class);
    }
}
