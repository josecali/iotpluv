<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoSensor extends Model
{
    protected $fillable = [
      'nombre_tipo_sensor', 'descripcion_tipo_sensor',
    ];
}
