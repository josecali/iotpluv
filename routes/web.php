<?php
use App\Lectura;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Hcomposer require "laravelcollective/html":"^5.2.0"ere is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {

  $numero = 2100;

    $chart = Charts::multi('line', 'material')
        // Setup the chart settings
        ->title("My Cool Chart")
        // A dimension of 0 means it will take 100% of the space
        ->dimensions(0, 400) // Width x Height
        // This defines a preset of colors already done:)
        ->template("material")
        // You could always set them manually
        // ->colors(['#2196F3', '#F44336', '#FFC107'])
        // Setup the diferent datasets (this is a multi chart)
        ->dataset('Element 1', [5,20,100])
        ->dataset('Element 2', [15,30,80])
        ->dataset('Element 3', [25,10,40])
        // Setup what the values mean
        ->labels(['One', 'Two', 'Three']);

    return view('index', [
      'chart' => $chart,
      'numero' => $numero
    ]);

});
  */
Route::get('/', 'ChartsController@graph');

Route::get('datagraph', function()
{
$idRandom = rand( 0,55);/*
  $chart = \DB::table('agua_clara1.lectura AS lectura')
              ->join('agua_clara1.estacion AS estacion', 'lectura.id_estacion', '=', 'estacion.id_estacion')
              ->select('estacion.nombre_estacion', 'estacion.id_estacion', 'lectura.id_estacion', 'lectura.valor', 'lectura.fecha', 'lectura.created_at')
              ->get();
*/
  $value = \DB::table('agua_clara1.lectura')->where('id_lectura', $idRandom)->value('valor');

  return ['value'=>intval($value)];
})->name('data');



Route::get('admin/actuators', 'ActuatorController@index');
Route::get('admin/actuators/create', 'ActuatorController@create');

Route::post('admin/actuators', 'ActuatorController@store');

Route::get('admin/actuators/{id}', 'ActuatorController@show');


Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/charts', 'ChartsController@index');
